package materialbibliografico.administrador.org.pl.material.dto;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import materialbibliografico.administrador.org.pl.material.bd.Conexion;
/*** added by dMaterialDAO* modified by dListarMaterial* modified by
dAgregarMaterial* modified by dModificarMaterial* modified by dEliminarMaterial
 */
public interface MaterialDAO {
	/*** added by dListarMaterial
	 */
	public Object [] [] obtenerTodas();
	/*** added by dListarMaterial
	 */
	public Material obtenerPorId(Integer id) throws Exception;
	/*** added by dAgregarMaterial
	 */
	public void agregar(Material c) throws Exception;
	/*** added by dModificarMaterial
	 */
	public void modificar(Material c) throws Exception;
	/*** added by dEliminarMaterial
	 */
	public void eliminar(Material c) throws Exception;
}