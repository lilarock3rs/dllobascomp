package materialbibliografico.administrador.org.pl.material.dto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import materialbibliografico.administrador.org.pl.material.bd.Conexion;
/*** added by dMaterialDAOMySQL* modified by dListarMaterial* modified by
dAgregarMaterial* modified by dModificarMaterial* modified by dEliminarMaterial
 */
public class MaterialDAOMySQL implements MaterialDAO {
	Statement st = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	Connection conn = null;
	Conexion conexion = null;
	/*** added by dListarMaterial
	 */
	public Object [] [] obtenerTodas() {
		try {
			int count1 = 0;
			conn = Conexion.getConexion();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM materialbibliografico");
			Object [] [] ArrayNew = new Object[30][5];
			while(rs.next()) {
				ArrayNew[count1][0] = rs.getInt(1);
				ArrayNew[count1][1] = rs.getString(2);
				ArrayNew[count1][2] = rs.getInt(3);
				ArrayNew[count1][3] = rs.getString(4);
				ArrayNew[count1][4] = rs.getInt(5);
				count1 ++;
			}
			conn.close();
			return ArrayNew;
		}
		catch(Exception e) {
			System.out.println(e + "");
		}
		finally {
		}
		return null;
	}
	/*** added by dListarMaterial
	 */
	public Material obtenerPorId(Integer id) throws SQLException {
		Material material = null;
		conn = Conexion.getConexion();
		String query = "SELECT * FROM materialbibliografico WHERE id=?";
		ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		rs = ps.executeQuery();
		if(rs.next()) {
			material = new Material(rs.getInt("id"), rs.getString("titulo"),
				rs.getInt("codigo"), rs.getString("autor"), rs.getInt("ano"));
		}
		conn.close();
		return material;
	}
	/*** added by dAgregarMaterial
	 */
	public void agregar(Material c) throws SQLException {
		conn = Conexion.getConexion();
		String query =
		"INSERT INTO materialbibliografico (titulo, codigo, autor, ano) values (?, ?)";
		ps = conn.prepareStatement(query);
		ps.setString(1, c.getTitulo());
		ps.setInt(2, c.getCodigo());
		ps.setString(3, c.getAutor());
		ps.setInt(4, c.getAno());
		ps.executeUpdate();
		conn.close();
	}
	/*** added by dModificarMaterial
	 */
	public void modificar(Material c) {
		try {
			conn = Conexion.getConexion();
			String query = "UPDATE categoria set nombre=?,descripcion=? WHERE id=?";
			ps = conn.prepareStatement(query);
			ps.setString(1, c.getTitulo());
			ps.setInt(2, c.getCodigo());
			ps.setString(3, c.getAutor());
			ps.setInt(4, c.getAno());
			ps.executeUpdate();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				conn.close();
			}
			catch(Exception e) {
			}
		}
	}
	/*** added by dEliminarMaterial
	 */
	public void eliminar(Material c) throws SQLException {
		conn = Conexion.getConexion();
		String query = "DELETE FROM materialbibliografico WHERE id=?";
		ps = conn.prepareStatement(query);
		ps.setInt(1, c.getId());
		ps.executeUpdate();
		conn.close();
	}
}