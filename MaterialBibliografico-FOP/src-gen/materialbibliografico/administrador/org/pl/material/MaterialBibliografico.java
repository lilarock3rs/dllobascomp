package materialbibliografico.administrador.org.pl.material;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import materialbibliografico.administrador.org.pl.material.bd.Conexion;
import materialbibliografico.administrador.org.pl.material.dto.Material;
import materialbibliografico.administrador.org.pl.material.dto.MaterialDAO;
/*** added by dMaterialBibliografico
 */
public class MaterialBibliografico extends JPanel {
	private MaterialDAO materialDAO;
	public MaterialBibliografico() {
		super(new GridLayout(2, 2));
		add();
	}
	public void add() {
		JTabbedPane tabbedPane = new JTabbedPane();
		Object [] [] data1 = null;
		String [] columnNames = {
			"ID", "TITULO", "CODIGO", "AUTOR", "A�O"
		};
		try {
			data1 = obtenerTodas();
		}
		catch(Exception e) {
			System.out.println(e);
		}
		JTable table = new JTable(data1, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		ImageIcon icon =
		createImageIcon("/MaterialBibliografico-FOP/images/middle.gif");
		JComponent panel1 = makeTextPanel("LISTAR");
		JComponent panel2 = makeTextPanel("AGREGAR");
		JComponent panel3 = makeTextPanel("MODIFICAR");
		JComponent panel4 = makeTextPanel("ELIMINAR");
		tabbedPane.addTab("Listar Material", icon, panel1);
		tabbedPane.addTab("Agregar Material", icon, panel2);
		tabbedPane.addTab("Modificar Material", icon, panel3);
		tabbedPane.addTab("Eliminar Material", icon, panel4);
		tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
		panel1.setLayout(new BorderLayout());
		panel1.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panel1.add(table, BorderLayout.CENTER);
		add(tabbedPane);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
	}
	public Object [] [] obtenerTodas() {
		Statement st = null;
		ResultSet rs = null;
		Connection conn = null;
		try {
			int count1 = 0;
			conn = Conexion.getConexion();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM materialbibliografico");
			Object [] [] ArrayNew = new Object[30][5];
			while(rs.next()) {
				ArrayNew[count1][0] = rs.getInt(1);
				ArrayNew[count1][1] = rs.getString(2);
				ArrayNew[count1][2] = rs.getInt(3);
				ArrayNew[count1][3] = rs.getString(4);
				ArrayNew[count1][4] = rs.getInt(5);
				count1 ++;
			}
			conn.close();
			return ArrayNew;
		}
		catch(Exception e) {
			System.out.println(e + "");
		}
		finally {
		}
		return null;
	}
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = MaterialBibliografico.class.getResource(path);
		if(imgURL != null) {
			return new ImageIcon(imgURL);
		}
		else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	public static void main(String [] args) {
		SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					createAndShowGUI();
				}
			});
	}
	private static void createAndShowGUI() {
		JFrame frame = new JFrame("Material Bibliogr�fico");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(new MaterialBibliografico(), BorderLayout.CENTER);
		frame.setPreferredSize(new Dimension(600, 500));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	protected JComponent makeTextPanel(String text) {
		JPanel panel = new JPanel(false);
		JLabel filler = new JLabel(text);
		filler.setHorizontalAlignment(JLabel.CENTER);
		panel.setLayout(new GridLayout(1, 1));
		panel.add(filler);
		return panel;
	}
}