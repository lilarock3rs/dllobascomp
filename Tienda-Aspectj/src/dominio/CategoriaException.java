package dominio;

import java.sql.SQLException;

public class CategoriaException extends SQLException {
private static final long serialVersionUID = 1L;
	
	public CategoriaException() {
		super("No existe el parametro ingresado.");
	}
	
	public CategoriaException(String message) {
		super(message);
	}
		
}
