package org.pl.eshop;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.pl.eshop.dto.*;

import dominio.CategoriaException;

public class Principal {

	public static void main(String[] args) throws SQLException {
		try {
		CategoriaDAOMySQL a = new CategoriaDAOMySQL();
		Categoria c = new Categoria (9, "Otro", "Otro");
		//Categoria x = new Categoria (9, "", "Insert");
		Categoria d = new Categoria (18, "Update", "Update");
		Categoria e = new Categoria (200, "Mala", "Mala");
		
		//A�adir item
		a.agregar(c);
		System.out.println("Se guardo en bd " + c.getDescripcion());
		//a.agregar(x);
		System.out.println();
		
		//Modificar item
		a.modificar(d);
		System.out.println("Se modifico en la categoria con el id " + d.getId());
		System.out.println();
		
		//Elimar item
		a.eliminar(e);
		System.out.println("Se elimino la categoria con el id " + c.getId());
		System.out.println();
		
		//Listar todo
		List<Categoria> lista = a.obtenerTodas();
		System.out.println("Listar todo: " + lista.size());
		
		Iterator<Categoria> it = lista.iterator();
		System.out.printf("%10s %20s %20s", "ID", "NOMBRE", "DESCRIPCION");
		System.out.println();
		System.out.println("-----------------------------------------------------------------------------");
		while ( it.hasNext() ) { 
		Object objeto = it.next(); 
		Categoria item = (Categoria)objeto;
		System.out.format("%10s %20s %20s \n", item.getId(), item.getNombre(), item.getDescripcion());
		}
	    System.out.println("-----------------------------------------------------------------------------");
	    
	    //Listar por id
	    System.out.println();
	    Categoria carga = a.obtenerPorId(11);
	    System.out.println("Mostrar informaci�n de la categor�a con el id "+ carga.getId());
	    System.out.println();
	    System.out.printf("%10s %20s %20s \n", "ID", "NOMBRE", "DESCRIPCION");
	    System.out.format("%10s %20s %20s", carga.getId(), carga.getNombre(), carga.getDescripcion());
	    
	    //Listar por id
	    System.out.println();
	    Categoria carga2 = a.obtenerPorId(3243);
	    
		
		
	} catch (CategoriaException e){
		System.out.println(e.getMessage());
		}
		
	}
	
	//Cuando se realiza una consulta a la base de datos.
	//Cuando ocurre una SQLException.
	//Cuando se elimina una categor�a.
}
