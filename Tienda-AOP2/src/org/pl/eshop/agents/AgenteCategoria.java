/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pl.eshop.agents;

import jade.core.Agent;

public class AgenteCategoria extends Agent {

    @Override
    protected void setup() {
        System.out.println("Bienvenido, el agente categoria esta listo.");
        addBehaviour(new AdminCategoriasAgregar());
    }
}
