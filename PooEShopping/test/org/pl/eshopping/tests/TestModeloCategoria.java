/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pl.eshopping.tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.pl.eshop.dto.Categoria;

@RunWith(Parameterized.class)
public class TestModeloCategoria {
private String n;
private String d;
@Parameters
 public static Collection<Object[]> data() {
 return Arrays.asList(new Object[][] {
 { "Noticias", "Bla" }, { "", ""}, { "Deportes", "" } , { "", "Bla" }
 });
 }

 public TestModeloCategoria(String nom, String desc) {
 n=nom;
 d=desc;
 }

 @Test
 public void test() {
 Categoria cat = new Categoria();
 try{
 cat.setNombre(n);
 cat.setDescripcion(d);
 }
 catch(IllegalArgumentException e){
 System.out.println(e.getMessage());
 org.junit.Assert.fail(e.getMessage());
 }
 }
 }